#include <iostream>
#include <conio.h>
#include <stdlib.h>
using namespace std;

class Node {
public:					//node has data for the name of the music and an int for its genre//
		string	data;
		int		group;
		Node*	next;
};
class List{
	public:
			List(void)	{head = NULL;	}
			~List(void);
			
			bool isEmpty(){ return head == NULL;	}
			Node* InsertNode(int index,string Name,int genre);
			int FindNode(string Name);
			int DeleteNode(int index);
			void DisplayList(void);
			string ViewMusic(int index);
			void DisplayPlaylist(void);
			int MaxCap(void);
			int ViewMusicGenre(int index);
			void DisplaybyGenre (int genrenum);
	private:
			Node* head;
			friend class Stack;
};
class Stack : public List {
	public:
		Stack(){}
		~Stack(){}
		string Top(){			// return head data ;cant return both data and genre//
			if (head == NULL){
				string err = "Error stack is Empty";
				return (err);
			}
			else{
				return (head->data);
			}
		}
		string TopGenre(){		//return genre; genre is indicated with number see code//
			string genre;
			if (head == NULL){
				string a ="";
				return (a);
			}
			else{
				if (head->group ==1 ){
				genre = "HipHop";
				}
				else if(head->group ==2 ){
				genre = "R&B";
				}
				else if (head->group ==3 ){
				genre = "Pop-Punk";
				}
				else if (head->group ==4 ){
				genre = "Alternative";
				}
				else if (head->group ==5 ){
				genre = "Pop";
				}
				else if (head->group ==6 ){
				genre = "Metal";
				}
				else if (head->group ==7 ){
				genre = "Pop-Rock";
				}
				else if (head->group ==8 ){
				genre = "Acoustic";
				}
				else if (head->group ==9 ){
				genre = "Reggae";
				}
				else if (head->group ==10 ){
				genre = "Emo";
				}
				else if (head->group ==11 ){
				genre = "Indie";
				}
				else if (head->group ==12 ){
				genre = "Rock";
				}
				else if (head->group ==13 ){
				genre = "Country";
				}
				return (genre);
			}
		}
		void Push(string x,int y) { InsertNode(0,x,y);} 	//uses insertnode from list//
		string Pop(){
			if (head == NULL){
				string a = "Error stack is Empty";
				return (a);
		}
		else {
			DeleteNode(0);	
			}
		}
		void DisplayStack() {DisplayPlaylist();}
};
class Queue : public List {
	public:
		Queue(){
			front = rear = NULL;
			counter = 0;
		}
		~Queue(){
			while (!IsEmpty()) Dequeue();
		}
		bool IsEmpty(){
			if (counter) 	return false;
			else 			return true;
		}
		void Enqueue(string x,int y);
		void Dequeue(void);
		void DisplayQueue(void);
	private:
		Node* front;
		Node* rear;
		int counter;
};
void Queue::Enqueue(string x,int y){   //puts the new node in the rear//
	Node* newNode = new Node;
	newNode->data = x;
	newNode->group = y;
	newNode->next = NULL;
	if (IsEmpty()){
		front = newNode;
		rear = newNode;
	}
	else{
		rear->next = newNode;
		rear = newNode;
	}
	counter++;
}
void Queue::Dequeue(){
	if(IsEmpty()){
		cout<<"The Queue is Empty"<<endl; //checks if queue is empty//
		return;
	}
		Node* nextNode = front->next; 
		delete front;
		front = nextNode;
		counter--;
}
void Queue::DisplayQueue(){
	string genre;
	Node* CurrNode = front;	
	for (int i = 0; i<counter; i++){
		if (CurrNode->group ==1 ){
		genre = "HipHop";
		}
		else if(CurrNode->group ==2 ){
		genre = "R&B";
		}
		else if (CurrNode->group ==3 ){
		genre = "Pop-Punk";
		}
		else if (CurrNode->group ==4 ){
		genre = "Alternative";
		}
		else if (CurrNode->group ==5 ){
		genre = "Pop";
		}
		else if (CurrNode->group ==6 ){
		genre = "Metal";
		}
		else if (CurrNode->group ==7 ){
		genre = "Pop-Rock";
		}
		else if (CurrNode->group ==8 ){
		genre = "Acoustic";
		}
		else if (CurrNode->group ==9 ){
		genre = "Reggae";
		}
		else if (CurrNode->group ==10 ){
		genre = "Emo";
		}
		else if (CurrNode->group ==11 ){
		genre = "Indie";
		}
		else if (CurrNode->group ==12 ){
		genre = "Rock";
		}
		else if (CurrNode->group ==13 ){
		genre = "Country";
		}
		cout<<CurrNode->data<<"\t"<<genre<<endl;
		CurrNode = CurrNode->next;
	}
}
int List::FindNode(string Name){		//finds a node data using string//
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrNode && CurrNode->data != Name){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrIndex;
	return 0;
}	
int List::MaxCap(){			//counts the maximum capacity of the list//
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		CurrNode	=	CurrNode->next;
		num++;
	}
	return (num);
} 
int List:: DeleteNode(int index){
	Node* prevNode	=	NULL;
	Node* CurrNode	=	head;
	int CurrIndex	=	0;
	while (CurrIndex != index){
		prevNode	=	CurrNode;
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode){
		if(prevNode){
			prevNode->next	=	CurrNode->next;
			delete CurrNode;
		}
		else{
			head	=	CurrNode->next;
			delete	CurrNode;
		}
		return	CurrIndex;
	}
	return 0;
}
void List::DisplayList(){			//display only the node data which is the music name with the artist//
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		cout<<CurrNode->data<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
	
}
void List::DisplayPlaylist(){		//the same with DisplayList but also includes the genre//
	int num = 1;
	string genre;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		if (CurrNode->group ==1 ){
			genre = "Pop";
		}
		else if(CurrNode->group ==2 ){
			genre = "Rock";
		}
		else if (CurrNode->group ==3 ){
			genre = "R&B";
		}
		cout<<"["<<num<<"]"<<CurrNode->data<<"\t\t\t"<<genre<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
}
Node* List::InsertNode(int index,string Name,int genre){ 
	if (index < 0) return NULL;
	int CurrIndex = 1;
	Node* CurrNode =head;
	while (CurrNode && index > CurrIndex){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	Node* newNode =		new		Node;
	newNode->data = 	Name;
	newNode->group = 	genre;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
	}
	else{
		newNode->next	=	CurrNode->next;
		CurrNode->next	=	newNode;
	}
	return newNode;
}
List::~List(void){
	Node* CurrNode	=	head,	*nextNode	=	NULL;
	while (CurrNode != NULL){
		nextNode	=	CurrNode->next;
		delete	CurrNode;
		CurrNode	=	nextNode;
	}
}
string List::ViewMusic(int index){  //returns Node data by using the index of a Node//
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrIndex != index){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrNode->data;
	return NULL;
}
int List::ViewMusicGenre(int index){ //returns Node group which is an int and is also the genre number//
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrIndex != index){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrNode->group;
}
void List::DisplaybyGenre(int genrenum){	//Displays the list by genre by using the displaylist code but adding an identifier to see if the input int is equal to the genre number chosen//
	int num = 0,gennum = 1;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		if (CurrNode->group == genrenum){
			cout<<"["<<gennum<<"]"<<CurrNode->data<<endl;
			CurrNode	=	CurrNode->next;
			num++;
			gennum++;
		}
		else{
			CurrNode	=	CurrNode->next;
			num++;
		}
	}
}